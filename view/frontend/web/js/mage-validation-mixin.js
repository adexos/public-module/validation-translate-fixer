define(['mage/translate', 'jquery'], function ($t, $) {

    return function (target) {

        $.validator.messages = $.extend($.validator.messages, {
            required: $.mage.__('This is a required field.'),
            remote: $.mage.__('Please fix this field.'),
            email: $.mage.__('Please enter a valid email address.'),
            url: $.mage.__('Please enter a valid URL.'),
            date: $.mage.__('Please enter a valid date.'),
            dateISO: $.mage.__('Please enter a valid date (ISO).'),
            number: $.mage.__('Please enter a valid number.'),
            digits: $.mage.__('Please enter only digits.'),
            creditcard: $.mage.__('Please enter a valid credit card number.'),
            equalTo: $.mage.__('Please enter the same value again.'),
            maxlength: $.validator.format($.mage.__('Please enter no more than {0} characters.')),
            minlength: $.validator.format($.mage.__('Please enter at least {0} characters.')),
            rangelength: $.validator.format($.mage.__('Please enter a value between {0} and {1} characters long.')),
            range: $.validator.format($.mage.__('Please enter a value between {0} and {1}.')),
            max: $.validator.format($.mage.__('Please enter a value less than or equal to {0}.')),
            min: $.validator.format($.mage.__('Please enter a value greater than or equal to {0}.'))
        });
    }
});