Adexos Valition translate fixer Module
===========================

Requirements
------------

* Magento >= 2.0 (CE && EE)

Installation
------------

The module can be installed with [composer](https://getcomposer.org/) with the following command:

```
composer require adexos/module-validation-translate-fixer
```

Purpose of module
-------------

This module is a workaround to fix the missed translation with mage/validation widget.
